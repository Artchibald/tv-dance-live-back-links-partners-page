console.log("Cables linked!");



//lazyload
(function() {
    function logElementEvent(eventName, element) {
        console.log(
            Date.now(),
            eventName,
            element.getAttribute("data-src")
        );
    }

    var callback_enter = function(element) {
        logElementEvent("🔑 ENTERED", element);
    };
    var callback_exit = function(element) {
        logElementEvent("🚪 EXITED", element);
    };
    var callback_reveal = function(element) {
        logElementEvent("👁️ REVEALED", element);
    };
    var callback_loaded = function(element) {
        logElementEvent("👍 LOADED", element);
    };
    var callback_error = function(element) {
        logElementEvent("💀 ERROR", element);
        element.src =
            "https://via.placeholder.com/440x560/?text=Error+Placeholder";
    };
    var callback_finish = function() {
        logElementEvent("✔️ FINISHED", document.documentElement);
    };

    ll = new LazyLoad({
        elements_selector: ".lazy",
        load_delay: 300,
        threshold: 0,
        // Assign the callbacks defined above
        callback_enter: callback_enter,
        callback_exit: callback_exit,
        callback_reveal: callback_reveal,
        callback_loaded: callback_loaded,
        callback_error: callback_error,
        callback_finish: callback_finish
    });
})();


//TVDANCE




jQuery(document).ready(function($) {


    //hero scroll fade


    $(window).scroll(function() {
        $(".fade-out").css("opacity", 1 - $(window).scrollTop() / 750);
    });


    //owl carousel
    $(".owl-carousel-1").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        lazyLoad: true,
        responsive: {
            0: {
                items: 2
            },
            767: {
                items: 3
            },
            990: {
                items: 4
            }
        }
    });

    $(".owl-carousel-2").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        lazyLoad: true,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 2
            },
            990: {
                items: 3
            }
        }
    });

    $(".owl-carousel-3").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        lazyLoad: true,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 1
            },
            990: {
                items: 1
            }
        }
    });

    $(".video-thumb").css("opacity", "0.00009");
    $(".video-thumb").trigger("pause");
    $(".video-thumb").mouseover(function() {
        console.log("hovered");
        $(this).css("opacity", "1");
        $(this).css("transition", "opacity 0.5s");
        $(this).trigger("play");
    });
    $(".video-thumb").mouseout(function() {
        console.log("left the hover");
        $(this).css("opacity", "0.00009");
        $(this).css("transition", "opacity 0.5s");
        $(this).trigger("pause");
    });
});